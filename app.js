// copy this into your app.js
const express = require('express');
const path = require('path');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

var Request = require("request");
const { cpuUsage } = require('process');

var isErrorNumbers = 0;
var isErrorWithin30 = false;
var apiResult;
getUrlResponse();

setInterval(() => {
    getUrlResponse();
}, 1000);



function getUrlResponse(){
    Request.get("https://dev.fractal-it.fr:8443/fake_health_test?dynamic=true", (error, response, body) => {
        if(error) {
            return console.dir(error);
        };
        // Lorsqu'il y a erreur pendant au moins 30 secondes
        if(isErrorNumbers === 30){
            var date = new Date();
            var current_time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
            console.dir(current_time + " Status à error depuis au moins 30s");
            isErrorWithin30 = true;
        }
        if(JSON.parse(body).status === 'error'){
            isErrorNumbers ++;
        }else{
            isErrorNumbers = 0;
            if(isErrorWithin30){
                var date = new Date();
                var current_time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                console.dir(current_time + " Status à ok de nouveau");
                isErrorWithin30 = false;
            }
        }
        apiResult = JSON.parse(body);;
    });
}

app.get('/', function(req, res, next) {
    res.render('index', { title: apiResult?.status });
});
app.get("/results",function(req,res){    
    res.writeHead(200,"OK",{"Content-Type":"text/html"});
    res.end(apiResult?.status);
});

module.exports = app;